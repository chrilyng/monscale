<%@ include file="/WEB-INF/views/includes/taglibs.jsp"%>
<div>
<c:choose><c:when test="${not empty results}"><div>Gears: <c:out value="${gears}"/></div><table id="measurements">
<c:forEach items="${results}" var="result"><tr><td><fmt:formatDate type="time"
                                                               value="${result.timeStamp}" />
                                </td><td><c:out value="${result.result}"/></td></tr>
                                </c:forEach>
                            </table></c:when><c:otherwise>No monitor messages found. Did you start the search?</c:otherwise></c:choose>
                            </div>