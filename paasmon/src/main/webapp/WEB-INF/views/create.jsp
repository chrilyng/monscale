<%@ include file="/WEB-INF/views/includes/taglibs.jsp"%>

<!DOCTYPE HTML>
<html>
    <head>

        <title>PAAS autoscaler</title>

        <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

        <link rel="stylesheet" href="<c:url value='/css/blueprint/screen.css'/>" type="text/css" media="screen, projection">
        <link rel="stylesheet" href="<c:url value='/css/blueprint/print.css'/>"  type="text/css" media="print">
<body>
    <div class="container">
        <form:form action="create" method="post" commandName="warForm" class="prepend-1 span-22 append-1 last">
            <div class="span-14">
                <label class="span-4" for="name">Application name:</label>
                <form:input type="text" path="name" id="name" class="span-8"/>
            </div>
            <div class="span-14">
                <label class="span-4" for="latency">Response time:</label>
                <form:input type="text" path="latency" id="latency" class="span-8"/>
            </div>
            <div class="span-14">
                <label class="span-4" for="minGears">Min gears:</label>
                <form:input type="text" path="minGears" id="minGears" class="span-8"/>
            </div>
            <div class="span-14">
                <label class="span-4" for="maxGears">Max gears:</label>
                <form:input type="text" path="maxGears" id="maxGears" class="span-8"/>
            </div>
            <div class="span-14">
                <label class="span-4" for="username">Username:</label>
                <form:input type="text" path="username" id="username" class="span-8"/>
            </div>
            <div class="span-14">
                <label class="span-4" for="password">Password:</label>
                <form:input type="password" path="password" id="password" class="span-8"/>
            </div>
            <div class="span-14">
                <label class="span-4" for="path">Application URL:</label>
                <form:input type="text" path="path" id="path" class="span-8"/>
            </div>
            <div class="span-14">
                <label class="span-4" for="scaleUtilityUrl">Scale service URL:</label>
                <form:input type="text" path="scaleUtilityUrl" id="scaleUtilityUrl" class="span-8"/>
            </div>
            <div class="span-4" style="float:right">
                <input type="submit" value="Create"/>
            </div>
        </form:form>
    </div>
</body>
</html>