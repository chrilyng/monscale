<%@ include file="/WEB-INF/views/includes/taglibs.jsp"%>

<!DOCTYPE HTML>
<html>
    <head>

        <title>PAAS autoscaler</title>

        <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

        <link rel="stylesheet" href="<c:url value='/css/blueprint/screen.css'/>" type="text/css" media="screen, projection">
        <link rel="stylesheet" href="<c:url value='/css/blueprint/print.css'/>"  type="text/css" media="print">
        <!--[if lt IE 8]>
            <link rel="stylesheet" href="/css/blueprint/ie.css" type="text/css" media="screen, projection">
        <![endif]-->
        <link rel="stylesheet" href="<c:url value='/css/main.css'/>"  type="text/css">

        <script src="<c:url value='/js/jquery-1.6.1.min.js'/>"></script>
        <script src="<c:url value='/js/jquery.periodicalupdater.js'/>"></script>

    <script src="<c:url value='/js/libraries/RGraph.common.core.js'/>" ></script>
    <script src="<c:url value='/js/libraries/RGraph.line.js'/>" ></script>
    </head>
    <body>
        <div class="container">
            <div id="header" class="prepend-1 span-22 append-1 last">
                <h1 class="loud">
                    Monitoring <span id="warName"><c:out value="${warName}"/></span>
                </h1>
                <div>
                    <form:form id="formId">
                        <input id="startMonitor" type="submit" name="startMonitor" value="Start monitor" /> |
                        <input id="stopMonitor" type="submit" name="stopMonitor"   value="Stop monitor" />
                    </form:form>
                </div>
            </div>
            <span id="content" class="prepend-1 span-6 append-1 prepend-top last" style="float:left;overflow: auto; height:600px;">
                <%@ include file="/WEB-INF/views/measurementMessages.jsp"%>
            </span>
            <span id="content2" class="prepend-1 span-14 append-1 prepend-top last" style="float:right;">
                <canvas id="cvs" width="600" height="600">[No canvas support]</canvas>
            </span>
        </div>




        <script type="text/javascript">
            var line;
            var gresults = [];
            for (var i=0; i<11; ++i) {
               gresults.push(null);
            }

            $(document).ready(
            function () {
               line = new RGraph.Line('cvs', gresults);
                line.set('labels', ['-9','-8','-7','-6','-5','-4','-3','-2','-1','0']);
                line.draw();
            });

            function drawGraph() {

                output = "";

                	gresults.push(parseInt(document.getElementById("measurements").rows[0].cells[1].innerHTML));

                    gresults.shift();


                RGraph.clear(document.getElementById("cvs"));
                //RGraph.reset(document.getElementById("cvs"));
                var line = new RGraph.Line('cvs', gresults);
                line.draw();
                //document.getElementById("gears").innerHTML=output;

            }

            $.PeriodicalUpdater('<c:url value="/results/ajax"/>', {
                        method: 'get', // method; get or post
                        data: '', // array of values to be passed to the page - e.g. {name: "John", greeting: "hello"}
                        minTimeout: 1000, // starting value for the timeout in milliseconds
                        maxTimeout: 20000, // maximum length of time between requests
                        multiplier: 2, // the amount to expand the timeout by if the response hasn't changed (up to maxTimeout)
                        type: 'text', // response type - text, xml, json, etc. See $.ajax config options
                        maxCalls: 0, // maximum number of calls. 0 = no limit.
                        autoStop: 0 // automatically stop requests after this many returns of the same data. 0 = disabled.
                    }, function(remoteData, success, xhr, handle) {
                        $('#content').html(remoteData);

                            drawGraph();
                });


            $(function() {
                $('#startTwitter').bind('click', function() {
                    $.post("<c:url value='/results'/>", "startMonitor=startMonitor");
                    return false;
                });

                $('#stopTwitter').bind('click', function() {
                    $.post("<c:url value='/results'/>", "stopMonitor=stopMonitor");
                    return false;
                });
            });

        </script>

    </body>
</html>
