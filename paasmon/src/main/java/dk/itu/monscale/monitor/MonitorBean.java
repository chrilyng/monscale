package dk.itu.monscale.monitor;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import dk.itu.monscale.analysis.MonitorResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.Date;
import java.util.LinkedList;

public class MonitorBean {
    private Collection<MonitorResult> results = new LinkedList<MonitorResult>();
    private String baseUrl = "http://trafficscale.cfapps.io";//"http://tomcat-chrilyng.rhcloud.com/trafficscale";//

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

        public String responseTime() {

            ClientConfig config = new DefaultClientConfig();
            Client client = Client.create(config);
            client.setConnectTimeout(0);
            WebResource service = client.resource(getBaseUrl());
            long startTime = System.currentTimeMillis();
            ClientResponse response = service.get(ClientResponse.class);


            if (response.getStatus() == 200) {

                MonitorResult result = new MonitorResult();

                long endTime = System.currentTimeMillis();
                long responseTime = endTime - startTime;
                result.setTimeStamp(new Date(startTime));
                result.setResult(responseTime);
                result.setMetric("response");

                logger.info(result.getTimeStamp().getTime() + ","+ result.getResult());

                results.add(result);
                return result.getResult()+"";
            }
            return null;
        }


    public Collection<MonitorResult> getResults() {
        return results;
    }

    public void setResults(Collection<MonitorResult> results) {
        this.results = results;
    }



    public String getBaseUrl() {
        return baseUrl;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }
}
