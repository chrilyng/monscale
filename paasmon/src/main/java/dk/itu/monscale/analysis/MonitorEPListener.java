package dk.itu.monscale.analysis;

import com.espertech.esper.client.EventBean;
import com.espertech.esper.client.UpdateListener;
import dk.itu.monscale.planning.WebController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import weka.classifiers.evaluation.NumericPrediction;
import weka.classifiers.functions.GaussianProcesses;
import weka.classifiers.timeseries.WekaForecaster;
import weka.core.*;

import java.util.List;

/**
 * Receives query results asynchronously from Esper engine and adds them to prediction service.
 *
 */


public class MonitorEPListener implements UpdateListener {
    private Instances bench;
    private final String ATTRIBUTE_RESPONSE = "response";

    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    WebController webController;

    public MonitorEPListener (WebController webController) {
        FastVector fvAttr = new FastVector(1);

        Attribute classAttribute = new Attribute(ATTRIBUTE_RESPONSE);

        fvAttr.addElement(classAttribute);

        bench = new Instances("benchmark", fvAttr, 0);

        this.webController = webController;
    }

    public Instance createNumericInstance(double input) {
        Instance thread = new DenseInstance(1);
        thread.setValue(0,input);
        return thread;
    }

    public double updateWithForecast() {

        try {

                WekaForecaster forecaster = new WekaForecaster();

                forecaster.setFieldsToForecast(ATTRIBUTE_RESPONSE);

                forecaster.setBaseForecaster(new GaussianProcesses());

                forecaster.buildForecaster(bench, System.out);

                forecaster.primeForecaster(bench);

                // match the lookahead with the time necessary for adding a 'gear'

                List<List<NumericPrediction>> forecast = forecaster.forecast(6, System.out);


                // get the last prediction
                List<NumericPrediction> resonseTimePred = forecast.get(0);

                int lastIndex = resonseTimePred.size()-1;
                double predictedResponse = resonseTimePred.get(lastIndex).predicted();

                return predictedResponse;

        } catch (Exception ex) {
            ex.printStackTrace();
            return 0d;
        }
    }

    @Override
    public void update(EventBean[] eventBeans, EventBean[] eventBeans2) {

        for(int i=0; i<eventBeans.length; i++) {
            Double averag = (Double)eventBeans[i].get("avg(result)");
            logger.info("EPMonitor " + averag + " added to list of size " + bench.size());
            if(averag!=null) {
                bench.add(createNumericInstance(averag));
                if(bench.size()>1) {
                    averag = updateWithForecast();
                }
                webController.forecast(averag);
            }
        }

    }
}