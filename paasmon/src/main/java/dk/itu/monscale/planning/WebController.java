package dk.itu.monscale.planning;

import com.espertech.esper.client.*;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.QueueingConsumer;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import dk.itu.monscale.analysis.MonitorEPListener;
import dk.itu.monscale.analysis.MonitorResult;
import dk.itu.monscale.monitor.MonitorBean;
import dk.itu.paasmodel.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import java.io.IOException;
import java.util.*;

@Controller
public class WebController extends WebMvcConfigurerAdapter {
    private double predictorThreshold;
    private int adoptionCost = 60000;
    private int violationCost = 15;
    private int cooldown = 120000;


    @Autowired
    private MessageChannel controlBusChannel;


    public void startMonitor() {
        Message<String> operation = MessageBuilder.withPayload("@mon.start()").build();
        controlBusChannel.send(operation);
    }


    public void stopMonitor() {
        Message<String> operation = MessageBuilder.withPayload("@mon.stop()").build();
        controlBusChannel.send(operation);
    }


    @Autowired
    private MonitorBean monitorCallback;

    private static final Logger logger = LoggerFactory.getLogger(WebController.class);
    private String scaleUtilityURL = "http://localhost:8080/paasscale/rest/scaleService/";
    private List<MonitorResult> monitorResults;
    private Stack<Double> results;

    private List<Double> historicalInput;
    private List<Double> predictedInput;
    private List<Double> historicalOutput;

    private Channel channel;
    private QueueingConsumer consumer;

    private int gears = 1;


    private List<Long> scaleChange;

    private float openshiftCost = 0.3f;
    private float pivotalCost = 0.4f;



    private WarForm currentWarForm;
    private GsonBuilder gsonBuilder = new GsonBuilder();
    private Gson gson = gsonBuilder.create();


    @RequestMapping(value="/")
    public String home(Model model, @RequestParam(required=false) String startMonitor,
                       @RequestParam(required=false) String stopMonitor
                       ) {
        return "home";
    }



    @RequestMapping(value="/create", method=RequestMethod.GET)
    public String warAppForm(Model model) {
        model.addAttribute("warForm",new WarForm());
        return "create";
    }

    @RequestMapping(value="/create", method=RequestMethod.POST)
    public String warappSubmit(@ModelAttribute(value="warForm") WarForm warForm) {
        monitorResults = new LinkedList<MonitorResult>();
        currentWarForm = warForm;

        results  = new Stack<Double>();

        scaleChange = new LinkedList<Long>();
        scaleChange.add(System.currentTimeMillis());


        EPServiceProvider epService = EPServiceProviderManager.getDefaultProvider();

        epService.getEPAdministrator().destroyAllStatements();

        String epl = "select avg(result) from dk.itu.monscale.analysis.MonitorResult.win:time(120 sec)";
        EPStatement statement = epService.getEPAdministrator().createEPL(epl);
        predictorThreshold =  currentWarForm.getLatency();

        MonitorEPListener latencyResultListener = new MonitorEPListener(this);

        statement.addListener(latencyResultListener);

        logger.info("Warapp created "+warForm.getLatency() + warForm.getName() + warForm.getPath());

        monitorCallback.setBaseUrl(warForm.getPath());

        scaleUtilityURL = warForm.getScaleUtilityUrl();

        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        Connection connection = null;

        try {
            connection = factory.newConnection();
            channel = connection.createChannel();
            consumer = new QueueingConsumer(channel);
        } catch (IOException e) {
            e.printStackTrace();
        }

        startMonitor();
        return "redirect:/results";
    }

    @RequestMapping(value="/results")
    public String homeresults(Model model, @RequestParam(required=false) String startMonitor,
                       @RequestParam(required=false) String stopMonitor) {
        if (startMonitor != null) {

            logger.info("start monitor");
            startMonitor();
            EPServiceProvider epService = EPServiceProviderManager.getDefaultProvider();
            epService.getEPAdministrator().startAllStatements();
            return "redirect:/results";
        }

        if (stopMonitor != null) {
            logger.info("stop monitor");
            stopMonitor();
            EPServiceProvider epService = EPServiceProviderManager.getDefaultProvider();
            epService.getEPAdministrator().stopAllStatements();

            return "redirect:/create";
        }

        model.addAttribute("warName", currentWarForm.getName());

        return "home";
    }

    public void forecast(double predictedResponse) {

        //double mpc = historicalInput.get(historicalInput.size()) + predictedInput.get(predictedInput.size()) + historicalOutput.get(historicalOutput.size());

        //do some cost analysis by finding last change
        // compare cost of changing with the benefit
        // history of scaling
        double cost = Math.pow((predictorThreshold-predictedResponse),2)/adoptionCost;

        long currentTime = System.currentTimeMillis();
        long historyTime = scaleChange.get(scaleChange.size() - 1);

        // change more than 2 mins ago
        if(cost>violationCost&&(currentTime - historyTime) > cooldown) {
            logger.info("Cost violation " + cost);

            CloudApp warApp = new CloudApp();
            warApp.setName(currentWarForm.getName());
            warApp.setPath(currentWarForm.getPath());
            Account account = new Account();
            account.setUsername(currentWarForm.getUsername());
            account.setPassword(currentWarForm.getPassword());
            warApp.setAccount(account);


            if (predictedResponse > predictorThreshold) {
                scaleUp(warApp);
                logger.info("Scale up " + predictedResponse);
            } else {
                scaleDown(warApp);
                logger.info("Scale down " + predictedResponse);
            }
        }
    }


    public void scaleUp(CloudApp warApp) {

        if(gears<currentWarForm.getMaxGears()) {

            ClientConfig config = new DefaultClientConfig();
            Client client = Client.create(config);
            client.setConnectTimeout(0);
            WebResource service = client.resource(scaleUtilityURL + "app/scaleUp");

            long currentTime = System.currentTimeMillis();


            scaleChange.add(currentTime);

            logger.info("Initiating scale up reqeust with gears: "+gears+","+currentTime);


            String body = gson.toJson(warApp);
            ClientResponse resp = service.entity(body).post(ClientResponse.class);

            String response = resp.getEntity(String.class);
            logger.info("Scale up response: "+response);

            if(resp.getStatus()==200) {
                //++gears;
                gears = gson.fromJson(response, Integer.class);

                logger.info("Scaling up with gears: "+gears + ","+currentTime);
            }
        }
    }


    public void scaleDown(CloudApp warApp) {
        if(gears>1) {
            ClientConfig config = new DefaultClientConfig();
            Client client = Client.create(config);
            client.setConnectTimeout(0);
            WebResource service = client.resource(scaleUtilityURL + "app/scaleDown");

            long currentTime = System.currentTimeMillis();

            scaleChange.add(currentTime);


            logger.info("Initiating scale down reqeust with gears: "+gears+","+currentTime);

            String body = gson.toJson(warApp);
            ClientResponse resp = service.entity(body).post(ClientResponse.class);

            String response = resp.getEntity(String.class);
            logger.info("Scale down response: "+response);

            if(resp.getStatus()==200) {
                //--gears;
                gears = gson.fromJson(response, Integer.class);
                logger.info("Scaling down with gears: "+gears+","+currentTime);
            }
        }
    }

    @RequestMapping(value="/results/ajax")
    public String ajaxCall(Model model) {

        try {
            channel.basicConsume("dk.itu.monscale.queue", true, consumer);

            QueueingConsumer.Delivery delivery = consumer.nextDelivery();
            String message = new String(delivery.getBody());

            MonitorResult result = new MonitorResult();
            result.setResult((Double.parseDouble(message)));
            result.setMetric("response");
            result.setTimeStamp(new Date(System.currentTimeMillis()));

            EPServiceProvider epService = EPServiceProviderManager.getDefaultProvider();
            epService.getEPRuntime().sendEvent(result);

            results.push(Double.parseDouble(message));

            Collections.reverse(monitorResults);
            monitorResults.add(result);
            Collections.reverse(monitorResults);

        } catch (Exception e) {
                e.printStackTrace();
        }
        model.addAttribute("results",monitorResults);
        model.addAttribute("gears", gears);

        return "measurementMessages";

    }
}
