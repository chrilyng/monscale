package dk.itu.monscale.planning;

public class WarForm {
    private String name;
    private String path;
    private String vendor;
    private String username;
    private String password;
    private int minGears;
    private int maxGears;
    private int latency;
    private String scaleUtilityUrl;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getVendor() {
        return vendor;
    }

    public void setVendor(String vendor) {
        this.vendor = vendor;
    }

    public int getLatency() {
        return latency;
    }

    public void setLatency(int latency) {
        this.latency = latency;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getMinGears() {
        return minGears;
    }

    public void setMinGears(int minGears) {
        this.minGears = minGears;
    }

    public int getMaxGears() {
        return maxGears;
    }

    public void setMaxGears(int maxGears) {
        this.maxGears = maxGears;
    }

    public String getScaleUtilityUrl() {
        return scaleUtilityUrl;
    }

    public void setScaleUtilityUrl(String scaleUtilityUrl) {
        this.scaleUtilityUrl = scaleUtilityUrl;
    }
}
