package dk.itu.paasmodel;

public interface ScaleUtility {
    public int scaleAppUp(CloudApp cloudApp);
    public int scaleAppDown(CloudApp cloudApp);
}
