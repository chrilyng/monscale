package dk.itu.paasscale;

import dk.itu.paasmodel.Account;
import org.cloudfoundry.client.lib.*;
import org.cloudfoundry.client.lib.CloudFoundryClient;
import org.cloudfoundry.client.lib.domain.CloudSpace;
import org.springframework.security.oauth2.common.OAuth2AccessToken;

import java.net.MalformedURLException;
import java.net.URL;

public class AccountManager {
    private final String cloudfoundryEndpoint = "https://api.run.pivotal.io";
    OAuth2AccessToken token = null;

    public CloudFoundryClient login(Account account) {

        CloudFoundryClient client = null;

        try {

            client = new CloudFoundryClient(new CloudCredentials(
                    account.getUsername(), account.getPassword()), new URL(cloudfoundryEndpoint));
            token = client.login();
            CloudSpace theCloudSpace=client.getSpaces().get(0);
            if (theCloudSpace==null)
                throw new IllegalArgumentException(theCloudSpace.getName());
            client = new CloudFoundryClient(new CloudCredentials(token), new URL(cloudfoundryEndpoint),theCloudSpace);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        return client;
    }


}
