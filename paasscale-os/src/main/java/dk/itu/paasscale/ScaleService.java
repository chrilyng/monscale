package dk.itu.paasscale;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.openshift.client.IOpenShiftConnection;
import com.openshift.client.IUser;
import com.openshift.client.OpenShiftConnectionFactory;
import dk.itu.paasmodel.CloudApp;
import dk.itu.paasmodel.ScaleUtility;
import org.cloudfoundry.client.lib.CloudFoundryClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;

@Path("scaleService")
public class ScaleService implements ScaleUtility {
    GsonBuilder gsonBuilder = new GsonBuilder();

    Gson gson = gsonBuilder.create();

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @POST
    @Consumes("text/*;charset=utf-8")
    @Produces("application/json")
    @Path("app/scaleUp")
    public String scaleUp(
            String cloudApp) {

        CloudApp target = gson.fromJson(cloudApp, CloudApp.class);

        int gears = 1;

        gears = scaleAppUp(target);

        return gson.toJson(gears);
    }


    @POST
    @Consumes("text/*;charset=utf-8")
    @Produces("application/json")
    @Path("app/scaleDown")
    public String scaleDown(
            String cloudApp) {

        CloudApp target = gson.fromJson(cloudApp, CloudApp.class);

        int gears = 1;


        gears = scaleAppDown(target);

        return gson.toJson(gears);
    }


    public int scaleAppUp(CloudApp cloudApp) {
        IOpenShiftConnection iOpenShiftConnection = new OpenShiftConnectionFactory()
                .getConnection(cloudApp.getAccount().getUsername(), cloudApp.getAccount().getUsername(), cloudApp.getAccount().getPassword());

        IUser user = iOpenShiftConnection.getUser();

        if (user.hasDomain()) {
            logger.info("Scale up openshift from gears: "+user.getConsumedGears());
            user.getDomains().get(0).getApplicationByName(cloudApp.getName()).scaleUp();
        }
        return user.getConsumedGears();
    }


    public int scaleAppDown(CloudApp cloudApp) {

        IOpenShiftConnection iOpenShiftConnection = new OpenShiftConnectionFactory()
                .getConnection(cloudApp.getAccount().getUsername(), cloudApp.getAccount().getUsername(), cloudApp.getAccount().getPassword());

        IUser user = iOpenShiftConnection.getUser();

        if (user.hasDomain()) {
            logger.info("Scale down openshift from gears: "+user.getConsumedGears());
            user.getDomains().get(0).getApplicationByName(cloudApp.getName()).scaleDown();
        }
        return user.getConsumedGears();
    }

}
